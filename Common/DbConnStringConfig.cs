﻿namespace Common;

public class DbConnStringConfig
{
    public string Host { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Database { get; set; }

    public string Value => $"Host={Host};Username={Username};Password={Password};Database={Database}";
}
