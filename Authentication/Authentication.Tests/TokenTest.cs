using System;
using FluentAssertions;
using JetBrains.Annotations;
using Xunit;

// ReSharper disable StringLiteralTypo

namespace Authentication.Tests;

[TestSubject(typeof(Token))]
public class TokenTest
{
    [Fact]
    public void TokenFunctionalityTest()
    {
        Token token =  new(Guid.NewGuid());
        Token.ValidateTokenString(token.Value).Should().BeTrue();
        Token.ValidateTokenString(token.Value+"0").Should().BeFalse();

        bool parsingResult = Token.TryParse(token.Value, out Token parsedToken);
        parsingResult.Should().BeTrue();
        token.PlainValue.Should().Be(parsedToken.PlainValue);

        string[] split = parsedToken.PlainValue.Split(Token.Separator);
        split.Should()
            .StartWith(Token.FirstElement)
            .And.HaveCount(Token.TokenParts);
        split[Token.GuidPart].Should().HaveLength(32);

        Guid guid = Guid.Parse(split[Token.GuidPart]);
        guid.Should().NotBeEmpty();

        int checksum = int.Parse(split[Token.ChecksumPart]);
        Token.Checksum(guid.ToString("N"), checksum).Should().BeTrue();
    }

    [Theory]
    [InlineData("opaque-12345678901234567890123456789000-135", true)]
    [InlineData("opaque-1234567890123456789012345678900a-145", true)]
    [InlineData("bpaque-1234567890123456789012345678900a-83", false)]
    [InlineData("opaque-1234567890123456789012345678900a", false)]
    public void ValidateTokenStringTest(string plainToken, bool valid)
    {
        string token = plainToken.ToBase64();
        Token.ValidateTokenString(token).Should().Be(valid);
    }
}
