﻿using System.Threading.Tasks;
using FluentAssertions;
using JetBrains.Annotations;
using Xunit;

namespace Authentication.Tests;

public class AuthenticationTest
{
    [Fact]
    public async Task TokenFlowTest()
    {
        AuthenticationFlow authFlow = new AuthenticationFlow(new InMemTokenPersistence());
        const string userOne = nameof(userOne);
        const string userTwo = nameof(userTwo);
        TokenPair tokenUser1 = await authFlow.Generate(userOne);
        (await authFlow.Verify(tokenUser1.Auth)).UserId.Should().BeEquivalentTo(userOne);

        UserCredential userCredential = await authFlow.Verify(tokenUser1.Auth);
        userCredential.Token.Refresh.Should().Be(tokenUser1.Refresh);

        TokenPair refreshedUser1 = await authFlow.Refresh(userOne, tokenUser1.Refresh);
        (await authFlow.Verify(tokenUser1.Auth)).UserId.Should().BeEmpty();
        (await authFlow.Verify(refreshedUser1.Auth)).UserId.Should().BeEquivalentTo(userOne);

        await authFlow.Expire(userOne, tokenUser1.Auth);
        (await authFlow.Verify(refreshedUser1.Auth)).UserId.Should().Be(userOne);

        await authFlow.Expire(userOne, refreshedUser1.Auth);
        (await authFlow.Verify(refreshedUser1.Auth)).UserId.Should().BeEmpty();
    }
}
