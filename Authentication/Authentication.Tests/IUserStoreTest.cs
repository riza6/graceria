﻿using System.Threading.Tasks;
using Authentication;
using FluentAssertions;
using JetBrains.Annotations;
using Xunit;

namespace Authentication.Tests;

[TestSubject(typeof(IUserStore))]
public class IUserStoreTest
{

    [Fact]
    public async Task UserStoreTest()
    {
        IUserStore store = new InMemUserStore();

        await store.RegisterNewUser(new User("::valid user::", "::valid password::"));
        bool isValidUserExist = await store.IsUserExist(new User("::valid user::", "::valid password::"));
        isValidUserExist.Should().BeTrue();

        bool invalidUserExist = await store.IsUserExist(new User("::invalid user::", "::valid password::"));
        invalidUserExist.Should().BeFalse();

        invalidUserExist = await store.IsUserExist(new User("::valid user::", "::invalid password::"));
        invalidUserExist.Should().BeFalse();


    }
}
