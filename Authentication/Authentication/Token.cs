﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Guid = System.Guid;

namespace Authentication;

public class Token
{
    public static readonly Token EmptyToken = new(Guid.Empty);

    public const int TokenParts = 3;
    public const int GuidPart = 1;
    public const int ChecksumPart = 2;
    public const string Separator = "-";
    public const string FirstElement = "opaque";

    public string PlainValue { get; }
    public string Value { get; }

    public Token(Guid guid)
    {
        string guidStr = guid.ToString("N");
        int checksum = SumGuid(guidStr);
        PlainValue = $"{FirstElement}-{guidStr}-{checksum}";
        Value = PlainValue.ToBase64();
    }

    public string GetShortPlainValue() => PlainValue.Substring(16) +"..etc";

    private static int SumGuid(string guid)
    {
        return guid.ToCharArray()
            .Select(c => int.Parse(c.ToString(), System.Globalization.NumberStyles.HexNumber))
            .Sum();
    }

    public static bool Checksum(string guid, int checksum) => SumGuid(guid) == checksum;

    public static bool TryParse(string tokenString, out Token result, ILogger<Token>? log = null)
    {
        result = EmptyToken;
        log ??= NullLogger<Token>.Instance;
        bool validationResult = ValidateTokenString(tokenString, log);
        if (!validationResult)
        {
            return false;
        }
        try
        {
            string decodedString = tokenString.FromBase64();

            // Assuming there's a check to ensure the split array has the necessary parts
            string[] parts = decodedString.Split(Separator);
            if (parts.Length <= GuidPart) // Replace 'GuidPart' with the correct index
            {
                return false;
            }

            Guid guid;
            if (!Guid.TryParse(parts[GuidPart], out guid)) // Replace 'GuidPart' with the correct index
            {
                return false;
            }

            result = new Token(guid);
            return true;
        }
        catch (Exception ex)
        {
            log.LogWarning(ex, "failed to parse token");
            return false;
        }
    }

    public static bool ValidateTokenString(string tokenString, ILogger<Token>? log = null)
    {
        log ??= NullLogger<Token>.Instance;
        if (tokenString.Length < FirstElement.Length + 16)
        {
            log.LogWarning("a base64 token wont be shorter than 16 + \"opaque\" length");
            return false;
        }

        try
        {
            string decoded = tokenString.FromBase64();
            if (!decoded.StartsWith(FirstElement))
            {
                log.LogWarning("a valid token start with "+ FirstElement);
                return false;
            }

            string[] parts = decoded.Split(Separator);
            if (parts.Length != TokenParts)
            {
                log.LogWarning("a valid token have 3 length");
                return false;
            }

            Guid.Parse(parts[GuidPart]);
            if (!int.TryParse(parts[ChecksumPart],  out int resultChecksum)) return false;
            if (!Checksum(parts[GuidPart], resultChecksum)) return false;
        }
        catch (Exception ex)
        {
            log.LogError(ex, "error while validate token");
            return false;
        }

        return true;
    }
}
