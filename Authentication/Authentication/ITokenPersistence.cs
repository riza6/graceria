﻿namespace Authentication;

public record TokenPair(Token Auth, Token Refresh)
{
    public static readonly TokenPair Empty = new(Token.EmptyToken, Token.EmptyToken);
}

public record UserCredential(string UserId, TokenPair Token)
{
    public static readonly UserCredential Empty = new (string.Empty, TokenPair.Empty);
    public bool AuthEquals(string plainValue) => Token.Auth.PlainValue.Equals(plainValue);
    public bool RefreshEquals(string plainValue) => Token.Refresh.PlainValue.Equals(plainValue);
}

public interface ITokenPersistence
{
    Task Save(UserCredential userCredential);
    Task<UserCredential> Get(Token token);
    Task<UserCredential> GetByUserId(string userId);
    Task Remove(UserCredential userCredential);
}
