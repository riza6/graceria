﻿using System.Security.Cryptography;
using System.Text;

namespace Authentication;

public static class StringExtension
{
    public static string ToBase64(this string plain)
    {
        byte[] byteArray = Encoding.UTF8.GetBytes(plain);
        return Convert.ToBase64String(byteArray);
    }

    public static string FromBase64(this string plain)
    {
        byte[] byteArray = Convert.FromBase64String(plain);
        return Encoding.UTF8.GetString(byteArray);
    }

    /// <summary>
    /// This method should yield same result if using query in PostgreSQL: SELECT encode(digest('::plain text here::', 'sha512'), 'hex');
    /// </summary>
    /// <param name="plain"></param>
    /// <returns></returns>
    public static string ToSHA512(this string plain)
    {
        byte[] hashBytes = SHA512.HashData(Encoding.UTF8.GetBytes(plain));
        return BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();
    }
}
