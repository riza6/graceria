﻿namespace Authentication;

public class InMemTokenPersistence : ITokenPersistence
{
    private readonly Dictionary<string, TokenPair> _map = new();
    private readonly Dictionary<string, string> _tokenMap = new();

    public Task Save(UserCredential userCredential)
    {
        _map[userCredential.UserId] = userCredential.Token;
        _tokenMap[userCredential.Token.Auth.PlainValue] = userCredential.UserId;
        return Task.CompletedTask;
    }

    public Task<UserCredential> Get(Token token)
    {
        bool getUserId = _tokenMap.TryGetValue(token.PlainValue, out string userId);
        if (!getUserId) return Task.FromResult(AuthenticationFlow.EmptyUserCredential);

        bool getUserInfo = _map.TryGetValue(userId, out TokenPair pair);
        if (!getUserInfo) return Task.FromResult(AuthenticationFlow.EmptyUserCredential);

        return Task.FromResult(new UserCredential(userId, pair));

    }

    public async Task<UserCredential> GetByUserId(string userId)
    {
        if (!_map.TryGetValue(userId, out TokenPair? value))
        {
            return AuthenticationFlow.EmptyUserCredential;
        }

        return await Task.FromResult(new UserCredential(userId, value));
    }

    public Task Remove(UserCredential userCredential)
    {
        _map.Remove(userCredential.UserId);
        return Task.CompletedTask;
    }
}
