﻿namespace Authentication;



public record User(string Username, string password);
public interface IUserStore
{
    Task RegisterNewUser(User user);
    Task<bool> IsUserExist(User user);
}

public class NullUserStore : IUserStore
{
    public static readonly IUserStore Instance = new NullUserStore();
    private NullUserStore(){ }
    public Task RegisterNewUser(User user) => Task.CompletedTask;
    public Task<bool> IsUserExist(User user) => Task.FromResult(false);
}


public class InMemUserStore : IUserStore
{
    private readonly Dictionary<string, string> _userPass = new ();

    public Task RegisterNewUser(User user)
    {
        if (!_userPass.ContainsValue(user.Username))
        {
            _userPass[user.Username] = user.password;
        }
        return Task.CompletedTask;
    }

    public Task<bool> IsUserExist(User user)
        => _userPass.ContainsKey(user.Username)
            ? Task.FromResult(_userPass[user.Username] == user.password)
            : Task.FromResult(false);
}
