﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Authentication;

public class AuthenticationFlow
{
    private static readonly Token EmptyToken = new (Guid.Empty);
    private static readonly TokenPair EmptyPair = new (EmptyToken, EmptyToken);
    public static readonly UserCredential EmptyUserCredential = new (string.Empty, EmptyPair);

    private readonly ITokenPersistence _tokenPersistence;
    private readonly ILogger<AuthenticationFlow> _log;

    public AuthenticationFlow(ITokenPersistence tokenPersistence,
        ILogger<AuthenticationFlow>? log = null)
    {
        _log = log ?? NullLogger<AuthenticationFlow>.Instance;
        _tokenPersistence = tokenPersistence;
    }

    /// <summary>
    /// after we authenticate user, we invoke this function to generate token
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    public async Task<TokenPair> Generate(string userId)
    {
        Token auth = new Token(Guid.NewGuid());
        Token refresh = new Token(Guid.NewGuid());
        TokenPair pair = new TokenPair(auth, refresh);
        UserCredential userCredential = new UserCredential(userId, pair);
        try
        {
            await _tokenPersistence.Save(userCredential);
        }
        catch (Exception e)
        {
            _log.LogError(e, "Error while persisting newly generated token for userid {UserId}", userId);
            throw;
        }

        return pair;
    }

    public async Task<UserCredential> Verify(Token auth)
    {
        try
        {
            UserCredential userCredential = await _tokenPersistence.Get(auth);
            return userCredential.AuthEquals(auth.PlainValue)
                ? userCredential
                : EmptyUserCredential;
        }
        catch (Exception e)
        {
            _log.LogError(e, "Error while Verify token {ShortPlainValue}", auth.GetShortPlainValue());
            return UserCredential.Empty;
        }
    }

    public async Task<TokenPair> Refresh(string userId, Token refresh)
    {
        try
        {
            UserCredential userCredential = await _tokenPersistence.GetByUserId(userId);
            return userCredential.RefreshEquals(refresh.PlainValue)
                ? await Generate(userId)
                : EmptyPair;
        }
        catch (Exception e)
        {
            _log.LogError(e, "Error while Refresh token userid {UserId} and token {ShortPlainValue}", userId, refresh.GetShortPlainValue());
            throw;
        }
    }

    public async Task Expire(string userId, Token auth)
    {
        try
        {
            UserCredential userCredential = await _tokenPersistence.GetByUserId(userId);
            if (userCredential.AuthEquals(auth.PlainValue))
            {
                await _tokenPersistence.Remove(userCredential);
            }
            else
            {
                _log.LogInformation($"no expiry because auth token not equal");
            }
        }
        catch (Exception e)
        {
            _log.LogError(e, "Error while Expire token userid {UserId} and token {ShortPlainValue}", userId, auth.GetShortPlainValue());
            throw;
        }
    }

    public async Task<bool> AuthenticateUser(string username, string cipherpassword)
    {
        return await Task.FromResult(true);
    }
}

