﻿namespace Web.Features.Echo;

public sealed class EchoProtectedEndpoint : EndpointWithoutRequest
{
    public override void Configure()
    {
        Get("/api/echo-protected");
    }

    public override async Task HandleAsync(CancellationToken c)
    {
        await SendAsync(new
        {
            UserId = User.Claims.FirstOrDefault(x => x.Type == "userId")?.ToString() ?? string.Empty
        }, cancellation: c);
    }
}
