﻿namespace Web.Features.Echo;

public class EchoRequest
{
    public Guid Guid { get; set; }
}

public class EchoResponse
{
    public Guid Guid { get; init; }
    public DateTime ReceivedAt { get; init; }
}

public class EchoEndpoint : Endpoint<EchoRequest, EchoResponse>
{
    public override void Configure()
    {
        Post("/api/echo");
        AllowAnonymous();
    }

    public override async Task HandleAsync(EchoRequest req, CancellationToken ct)
    {
        EchoResponse response = new EchoResponse{Guid = req.Guid, ReceivedAt = DateTime.Now};
        await SendAsync(response);
    }
}
