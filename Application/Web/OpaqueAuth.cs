﻿using System.Security.Claims;
using System.Security.Principal;
using System.Text.Encodings.Web;
using Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using UserManagement;

namespace Web;

public class OpaqueAuth : AuthenticationHandler<AuthenticationSchemeOptions>
{
    internal const string SchemeName = "Opaque";
    internal const string AuthHdrName = "authorization";
    private const int MinimumTokenLength = 6;


    private readonly IUserDirectory _userDirectory;
    private readonly AuthenticationFlow _authFlow;

    public OpaqueAuth(IOptionsMonitor<AuthenticationSchemeOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        AuthenticationFlow authFlow,
        IUserDirectory userDirectory,
        ISystemClock clock) : base(options, logger, encoder, clock)
    {
        _authFlow = authFlow;
        _userDirectory = userDirectory;
    }

    private bool IsPublicEndpoint() => Context
        .GetEndpoint()?
        .Metadata.OfType<AllowAnonymousAttribute>()
        .Any() is null or true;

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        if (IsPublicEndpoint())
            return AuthenticateResult.NoResult();

        bool hdrIsPresent = Request.Headers.TryGetValue(AuthHdrName, out StringValues tokenStr);
        if (!hdrIsPresent)
            return AuthenticateResult.NoResult();

        if(tokenStr.Count > 0 && tokenStr.ToString().TrimStart().Length < MinimumTokenLength)
            return AuthenticateResult.NoResult();

        bool parseTokenResult = Token.TryParse(TrimBearer(tokenStr), out Token token);
        if(!parseTokenResult)
            return AuthenticateResult.NoResult();

        UserCredential userCredential = await _authFlow.Verify(token);
        if (userCredential == UserCredential.Empty)
            return AuthenticateResult.NoResult();

        UserInfo userInfo = await _userDirectory.FetchUser(userCredential.UserId);
        if (userInfo == UserInfo.Empty)
            return AuthenticateResult.NoResult();

        IEnumerable<Claim> claims = userInfo.ConvertAttributes((key, value) => new Claim(key, value));
        var principal = new GenericPrincipal(
            new ClaimsIdentity (claims: claims, authenticationType: Scheme.Name),
            userInfo.Roles.ToArray()
        );

        return AuthenticateResult.Success(new AuthenticationTicket(principal, Scheme.Name));
    }

    private static string TrimBearer(StringValues tokenStr)
    {
        return tokenStr.ToString().TrimStart().TrimEnd().Substring(MinimumTokenLength).TrimStart();
    }
}
