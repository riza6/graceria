global using FastEndpoints;
global using FluentValidation;
using FastEndpoints.Swagger;
using FastEndpoints.Security;
using Microsoft.AspNetCore.Authentication;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddFastEndpoints()
    .AddAuthorization()
    .AddAuthentication(Web.OpaqueAuth.SchemeName)
    .AddScheme<AuthenticationSchemeOptions, Web.OpaqueAuth>(Web.OpaqueAuth.SchemeName, null);
builder.Services
    .SwaggerDocument();

var app = builder.Build();
app.UseFastEndpoints()
    .UseSwaggerGen();

app.Run();
