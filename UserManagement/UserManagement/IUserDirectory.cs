﻿using System.Collections.Immutable;
using System.Linq;

namespace UserManagement;

public class UserInfo
{
    public static readonly UserInfo Empty = new (string.Empty, Array.Empty<string>(), new List<(string Key, string Value)>());

    public string UserId { get; }
    public ImmutableList<string> Roles { get; }
    private ImmutableList<(string Key, string Value)> Attributes { get; }

    public UserInfo(string userId, IEnumerable<string> roles, List<(string, string)> attributes)
    {
        UserId = userId;
        Roles = roles.ToImmutableList();
        Attributes = attributes.ToImmutableList();
    }

    public IEnumerable<T> ConvertAttributes<T>(Func<string, string, T> factory)
    {
        return Attributes.Select(x => factory(x.Key, x.Value));
    }
}


public interface IUserDirectory
{
    Task<UserInfo> FetchUser(string userId);
    Task AddUser(UserInfo userInfo);
}
