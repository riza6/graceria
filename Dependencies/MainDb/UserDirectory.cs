﻿using Microsoft.EntityFrameworkCore;
using UserManagement;

namespace MainDb;

public partial class AppDbContext : IUserDirectory
{
    public async Task<UserInfo> FetchUser(string userId)
    {
        User? user = await Users.FirstOrDefaultAsync(u => u.Id == userId);
        if(user is null)
            return UserInfo.Empty;

        await UserRoles.Include(ur => ur.Role)
            .Where(r => r.UserId == userId).ToListAsync();

        return new UserInfo(userId, null, null);
    }

    public Task AddUser(UserInfo userInfo)
    {
        throw new NotImplementedException();
    }
}
