﻿using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace MainDb;

public partial class AppDbContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<UserRole> UserRoles { get; set; }

    public string DbPath { get; }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSnakeCaseNamingConvention();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        // Set the schema for all tables in the DbContext
        modelBuilder.HasDefaultSchema("user_management");
    }
}

public class User
{
    public string Id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public Dictionary<string,string> Attributes { get; set; }
}

public class Role
{
    public string Id { get; set; }
    public string Name => Id;
    public Dictionary<string,string> Attributes { get; set; }
}

public class UserRole
{
    public Guid Id { get; set; }
    public string UserId { get; set; }
    public User User { get; set; }
    public string RoleId { get; set; }
    public Role Role { get; set; }
}

