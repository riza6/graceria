﻿using Common;

namespace MainDb;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
{
    public AppDbContext CreateDbContext(string[] args)
    {
        string root = Directory.GetCurrentDirectory();
        string dotenv = Path.Combine(root, ".env");
        EnvironmentModifier.Load(dotenv);
        DbConnStringConfig config = new()
        {
            Host = Environment.GetEnvironmentVariable(nameof(DbConnStringConfig.Host)),
            Username = Environment.GetEnvironmentVariable(nameof(DbConnStringConfig.Username)),
            Password = Environment.GetEnvironmentVariable(nameof(DbConnStringConfig.Password)),
            Database = Environment.GetEnvironmentVariable(nameof(DbConnStringConfig.Database)),
        };

        var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
        optionsBuilder.UseNpgsql(config.Value);

        return new AppDbContext(optionsBuilder.Options);
    }
}
