﻿using Dapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Npgsql;
using Authentication;

namespace Postgresql;

internal class TokenSelectOutput
{
    public string user_id { get; set; }
    public string auth_token { get; set; }
    public string refresh_token { get; set; }
}

internal class TokenUpsertInput
{
    public string UserId { get; set; }
    public string Auth { get; set; }
    public string Refresh { get; set; }
}

/// <summary>
/// constructor in this class is private, look for Create method instead to instantiate
/// </summary>
public class PgTokenPersistence : ITokenPersistence
{
    private const string CreateTableCommand =
        """
            CREATE TABLE IF NOT EXISTS user_info (
                user_id VARCHAR(64) PRIMARY KEY,
                auth_token VARCHAR(128),
                refresh_token VARCHAR(128)
            );
        """;

    private const string TokenUpsert =
        """
            INSERT INTO user_info (user_id, auth_token, refresh_token) VALUES (@UserId, @Auth, @Refresh)
            ON CONFLICT (user_id) DO UPDATE
            SET auth_token = EXCLUDED.auth_token, refresh_token = EXCLUDED.refresh_token;
        """;

    private const string TokenSelectByUserId =
        """
            SELECT user_id, auth_token, refresh_token  FROM user_info WHERE user_id = @UserId;
        """;

    private const string TokenSelectByAuthToken =
        """
            SELECT user_id, auth_token, refresh_token  FROM user_info WHERE auth_token = @AuthToken;
        """;

    private const string TokenDelete =
        """
            DELETE FROM user_info WHERE user_id = @UserId;
        """;

    private readonly NpgsqlDataSource _dataSource;
    private readonly ILogger<PgTokenPersistence> _log;

    private PgTokenPersistence(string connStr, ILogger<PgTokenPersistence> log)
    {
        _dataSource = NpgsqlDataSource.Create(connStr);
        _log = log;
    }

    public async Task Save(UserCredential userCredential)
    {
        await using NpgsqlConnection conn = _dataSource.CreateConnection();
        await conn.ExecuteScalarAsync(TokenUpsert, new TokenUpsertInput
        {
            UserId = userCredential.UserId,
            Auth = userCredential.Token.Auth.Value,
            Refresh = userCredential.Token.Refresh.Value
        });
    }

    public async Task<UserCredential> Get(Token token)
    {
        bool parseAuthTokenSuccess = Token.TryParse(token.Value, out Token auth);
        if (!parseAuthTokenSuccess)
            return AuthenticationFlow.EmptyUserCredential;

        try
        {
            await using NpgsqlConnection conn = _dataSource.CreateConnection();
            TokenSelectOutput output = await conn.QuerySingleAsync<TokenSelectOutput>(TokenSelectByAuthToken, new{ AuthToken = token.Value});
            bool parseRefreshTokenSuccess = Token.TryParse(output.refresh_token, out Token refresh);
            return parseRefreshTokenSuccess
                ? new UserCredential(output.user_id, new TokenPair(auth, refresh))
                : AuthenticationFlow.EmptyUserCredential;
        }
        catch (Exception e)
        {
            _log.LogWarning(e, "failed to get UserInfo from Token {GetShortPlainValue}", token.GetShortPlainValue());
            return AuthenticationFlow.EmptyUserCredential;
        }
    }

    public async Task<UserCredential> GetByUserId(string userId)
    {
        try
        {
            await using NpgsqlConnection conn = _dataSource.CreateConnection();
            TokenSelectOutput output = await conn.QuerySingleAsync<TokenSelectOutput>(TokenSelectByUserId, new{ UserId = userId});
            bool parseAuthTokenSuccess = Token.TryParse(output.auth_token, out Token auth);
            bool parseRefreshTokenSuccess = Token.TryParse(output.refresh_token, out Token refresh);
            switch (parseAuthTokenSuccess && parseRefreshTokenSuccess)
            {
                case true : return new UserCredential(output.user_id, new TokenPair(auth, refresh));
                case false:
                    if (!parseAuthTokenSuccess) _log.LogWarning("failed to parse auth token from db");
                    if (!parseRefreshTokenSuccess) _log.LogWarning("failed to parse refresh token from db");
                    return AuthenticationFlow.EmptyUserCredential;
            }
        }
        catch (Exception e)
        {
            _log.LogWarning(e, "failed to get UserInfo of user {UserId}", userId);
            return AuthenticationFlow.EmptyUserCredential;
        }
    }

    public async Task Remove(UserCredential userCredential)
    {
        await using NpgsqlConnection conn = _dataSource.CreateConnection();
        await conn.ExecuteScalarAsync(TokenDelete, userCredential);
    }

    public static async Task<ITokenPersistence> Create(string connStr, ILogger<PgTokenPersistence>? log = null)
    {
        log ??= NullLogger<PgTokenPersistence>.Instance;
        try
        {
            await using var conn = new NpgsqlConnection(connStr);
            await conn.ExecuteAsync(CreateTableCommand);
            return new PgTokenPersistence(connStr, log);
        }
        catch (Exception e)
        {
            log.LogError(e, $"cannot connect to database with conn {connStr}");
            throw;
        }
    }
}
