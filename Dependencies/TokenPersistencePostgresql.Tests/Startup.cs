﻿using System;
using System.IO;
using Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Postgresql.Tests;

public class Startup
{
    public void ConfigureHost(IHostBuilder hostBuilder)
    {
        string root = Directory.GetCurrentDirectory();
        string dotenv = Path.Combine(root, ".env");
        EnvironmentModifier.Load(dotenv);

        var config = new ConfigurationBuilder()
            .AddEnvironmentVariables()
            .Build();

        hostBuilder.ConfigureHostConfiguration(builder => builder.AddConfiguration(config));
    }
}
