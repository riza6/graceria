﻿using System;
using System.Threading.Tasks;
using Common;
using Dapper;
using FluentAssertions;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Authentication;
using Xunit;

namespace Postgresql.Tests;


[TestSubject(typeof(PgTokenPersistence))]
public class PgTokenPersistenceTest : IAsyncLifetime
{
    private readonly DbConnStringConfig _connString = new();
    private readonly string _schemaName = "pgtokenpersistencetest_" + Guid.NewGuid().ToString("N");


    public PgTokenPersistenceTest(IConfiguration config)
    {
        config.Bind(_connString);
    }

    public async Task InitializeAsync()
    {
        await using var conn = new NpgsqlConnection(_connString.Value);
        await conn.ExecuteAsync($"CREATE SCHEMA {_schemaName}; ");
    }

    [Fact]
    public async Task TokenPersistenceTest()
    {
        ITokenPersistence tokenPersistence = await PgTokenPersistence.Create($"{_connString.Value};Search Path={_schemaName}");

        UserCredential userCredential = await tokenPersistence.GetByUserId("::a user::");
        userCredential.Should().BeEquivalentTo(AuthenticationFlow.EmptyUserCredential);

        TokenPair tokenPair = new TokenPair(new Token(Guid.NewGuid()), new Token(Guid.NewGuid()));
        await tokenPersistence.Save(new UserCredential("::a user::", tokenPair));

        UserCredential retrieved = await tokenPersistence.Get(tokenPair.Auth);
        retrieved.Token.Should().BeEquivalentTo(tokenPair);
        retrieved.UserId.Should().BeEquivalentTo("::a user::");

        retrieved = await tokenPersistence.GetByUserId("::a user::");
        retrieved.Token.Should().BeEquivalentTo(tokenPair);
        retrieved.UserId.Should().BeEquivalentTo("::a user::");

        UserCredential notFound = await tokenPersistence.GetByUserId("::not found user::");
        notFound.Should().BeEquivalentTo(AuthenticationFlow.EmptyUserCredential);

        await tokenPersistence.Remove(retrieved);
        retrieved = await tokenPersistence.GetByUserId("::a user::");
        retrieved.Should().BeEquivalentTo(AuthenticationFlow.EmptyUserCredential);
    }

    public async Task DisposeAsync()
    {
        await using var conn = new NpgsqlConnection(_connString.Value);
        await conn.ExecuteAsync($"DROP SCHEMA IF EXISTS {_schemaName} CASCADE; ");
    }
}
