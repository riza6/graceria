﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Configurations;
using DotNet.Testcontainers.Containers;
using FluentAssertions;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Testcontainers.PostgreSql;
using UserManagement;
using Xunit;

namespace MainDb.Tests;

[TestSubject(typeof(AppDbContext))]
public class UserDirectoryTest: IAsyncLifetime
{
    private readonly PostgreSqlContainer _dbContainer;


    private AppDbContext _dbContext { get; set; }

    public UserDirectoryTest()
    {
        _dbContainer = new PostgreSqlBuilder()
            .WithImage("postgres:14.7")
            .WithDatabase("db")
            .WithUsername("postgres")
            .WithPassword("postgres")
            .WithCleanUp(true)
            .Build();
    }

    public async Task InitializeAsync()
    {
        await _dbContainer.StartAsync();
        var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseNpgsql(_dbContainer.GetConnectionString())
            .Options;

        _dbContext = new AppDbContext(options);

        // Optionally, apply migrations if necessary
        await _dbContext.Database.MigrateAsync();
    }

    public async Task DisposeAsync()
    {
        await _dbContext.DisposeAsync();
        await _dbContainer.DisposeAsync();
    }


    [Fact]
    public async Task UserDirectoryAddFetchTest()
    {
        IUserDirectory ud = _dbContext;
        UserInfo userInfo = new UserInfo("::a user::",
            new[] {"::a role::"},
            new List<(string, string)>
            {
                ("::a key::", "::a value::"),
                ( "attribute2", "value2")
            });
        UserInfo fetchResult = await ud.FetchUser(userInfo.UserId);
        fetchResult.Should().BeEquivalentTo(UserInfo.Empty);

        await ud.AddUser(userInfo);
        fetchResult = await ud.FetchUser(userInfo.UserId);
        fetchResult.Should().BeEquivalentTo(userInfo);

        fetchResult = await ud.FetchUser("::invalid user::");
        fetchResult.Should().BeEquivalentTo(UserInfo.Empty);
    }
}
